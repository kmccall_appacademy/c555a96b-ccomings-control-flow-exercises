# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  idx = 0
  while idx < str.length
    if str[idx] == str[idx].downcase
      str[idx] = ''
    end
    idx += 1
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.even?
    str[str.length / 2 - 1] + str[str.length / 2]
  else
    str[str.length / 2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  vowels = 'aeiuoAEIOU'
  count = 0
  idx = 0
  while idx < str.length
    count += 1 if vowels.include?(str[idx])
    idx += 1
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  product = 1
  idx = 2
  while idx <= num
    product *= idx
    idx += 1
  end
  product
end

# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  str = ''
  idx = 0
  while idx < arr.length
    str << arr[idx] + separator unless idx == arr.length - 1
    if idx == arr.length - 1
      str << arr[idx]
    end
    idx += 1
  end
  str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  idx = 0
  new = ''
  while idx < str.length
    if idx.even?
      new << str[idx].downcase
    else
      new << str[idx].upcase
    end
    idx += 1
  end
  new
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  arr = str.split
  arr.each do |word|
    word.reverse! if word.length >= 5
  end
  arr.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  idx = 1
  arr = []
  while idx <= n
    if idx % 15 == 0
      arr << 'fizzbuzz'
    elsif idx % 5 == 0
      arr << 'buzz'
    elsif idx % 3 == 0
      arr << 'fizz'
    else
      arr << idx
    end
    idx += 1
  end
  arr
end

# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  new_arr = []
  idx = arr.length - 1
  while idx >= 0
    new_arr << arr[idx]
    idx -= 1
  end
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  factor = 1
  count = 0
  while factor <= num
    count += 1 if num % factor == 0
    factor += 1
  end

  if count == 2
    return true
  else
    return false
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  arr = []
  count = 1
  while count <= num
    if num % count == 0
      arr << count
    end
    count += 1
  end
  arr

  # arr = []
  # factor = 1
  # while factor <= num
  #   if num % factor == 0 && prime?(factor) == true
  #     arr << factor
  #   end
  #   factor += 1
  # end
  # arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  arr = []
  factor = 1
  while factor <= num
    if num % factor == 0 && prime?(factor) == true
      arr << factor
    end
    factor += 1
  end
  arr
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  arr = []
  factor = 1
  while factor <= num
    if num % factor == 0 && prime?(factor) == true
      arr << factor
    end
    factor += 1
  end
  arr.length
end

# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odd_arr = []
  even_arr = []
  idx = 0
  while idx < arr.length
    if arr[idx].even?
      even_arr << arr[idx]
    else
      odd_arr << arr[idx]
    end
    idx += 1
  end
  if even_arr.length == 1
    return even_arr[0]
  else
    return odd_arr[0]
  end
end
